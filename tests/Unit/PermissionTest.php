<?php

namespace Sentrio\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Sentrio\Database\Models\AdminUser;
use Sentrio\Database\Models\Permission;
use Sentrio\Tests\TestCase;

class PermissionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function permission_test()
    {
        $this->createAdminUser(['is_super_admin' => 0])
            ->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        $this->createPermissionForUser($this->user, 'admin.dashboard');

        $this->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * Create a permission for the user
     *
     * @param  \Sentrio\Database\Models\AdminUser $user
     * @param  string                             $name
     * @return void
     */
    protected function createPermissionForUser(AdminUser $user, string $name)
    {
        $permission = new Permission(['name' => $name]);

        $user->role->permissions()->save($permission);
        $user->load('role.permissions');
    }
}
