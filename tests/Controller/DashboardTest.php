<?php

namespace Sentrio\Tests\Controller;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Sentrio\Tests\TestCase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_returns_dashboard_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(200);
    }
}
