<?php

namespace Sentrio\Tests;

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Notification;
use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Sentrio\Database\Contracts\CurrencyInterface;
use Sentrio\Database\Models\AdminUser;
use Sentrio\Database\Models\Currency;
use Sentrio\SentrioServiceProvider;

class TestCase extends OrchestraTestCase
{
    /**
     * The admin user instance
     *
     * @var \Sentrio\Database\Models\AdminUser
     */
    protected $user;

    /**
     * The faker generator instance
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Setup the test environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->faker = $this->app->make(Faker::class);

        $this->withFactories(__DIR__.'/../database/factories');

        $this->setUpDatabase();
        $this->setDefaultCurrency();

        Notification::fake();
    }

    /**
     * Get package providers
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            SentrioServiceProvider::class
        ];
    }

    /**
     * Define environment setup
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.key', 'base64:UTyp33UhGolgzCK5CJmT+hNHcA+dJyp3+oINtX+VoPI=');
    }

    /**
     * Get package aliases
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'Breadcrumb' => \Sentrio\Support\Facades\Breadcrumb::class,
            'Cart' => \Sentrio\Support\Facades\Cart::class,
            'Menu' => \Sentrio\Support\Facades\Menu::class,
            'Module' => \Sentrio\Support\Facades\Module::class,
            'Permission' => \Sentrio\Support\Facades\Permission::class,
            'Shipping' => \Sentrio\Support\Facades\Shipping::class,
            'Tab' => \Sentrio\Support\Facades\Tab::class,
            'Widget' => \Sentrio\Support\Facades\Widget::class
        ];
    }

    protected function setUpDatabase(): void
    {
        $this->loadLaravelMigrations();
        $this->resetDatabase();
    }

    protected function resetDatabase(): void
    {
        $this->artisan('migrate');
    }

    protected function setDefaultCurrency()
    {
        factory(Currency::class)->create();

        $currencyRepository = app(CurrencyInterface::class);
        $currency = $currencyRepository->all()->first();

        $this->withSession(['default_currency' => $currency]);
    }

    /**
     * Create admin user
     *
     * @param  array $data
     * @return $this
     */
    protected function createAdminUser($data = ['is_super_admin' => 1]): self
    {
        if (null === $this->user) {
            $this->user = factory(AdminUser::class)->create($data);
        }

        return $this;
    }
}
