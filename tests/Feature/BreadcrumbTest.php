<?php

namespace Sentrio\Tests\Feature;

use Illuminate\View\View;
use Sentrio\Breadcrumbs\Builder;
use Sentrio\Tests\TestCase;

/** @runInSeparateProcess */
class BreadcrumbTest extends TestCase
{
    /** @test */
    public function breadcrumb_builder_test()
    {
        $builder = new Builder();
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->route('test.route');
        });

        $testRoute = $builder->get('test.route');
        $this->assertEquals($testRoute->route(), 'test.route');
    }

    /** @test */
    public function breadcrumb_builder_render_test()
    {
        $builder = new Builder();
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->route('test.route');
        });

        $this->assertInstanceOf(View::class, $builder->render('test.route'));
    }

    /** @test */
    public function breadcrumb_builder_label_test()
    {
        $builder = new Builder();
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->label('test label');
        });
        $testRoute = $builder->get('test.route');

        $this->assertEquals($testRoute->label(), 'test label');
    }

    /** @test */
    public function breadcrumb_parent_test()
    {
        $builder = new Builder();
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->label('test parent label');
        });

        $builder->make('test.child.route', function ($breadcrumb) {
            $breadcrumb->label('test label')
                ->parent('test.route');
        });
        $testRoute = $builder->get('test.child.route');

        $this->assertEquals($testRoute->parents->count(), 1);
    }
}
