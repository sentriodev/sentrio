<?php

namespace Sentrio\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Sentrio\Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_login_route_returns_correct_view_test()
    {
        $this->get(route('admin.login'))
            ->assertStatus(200)
            ->assertViewIs('sentrio::auth.login');
    }

    /** @test */
    public function admin_logout_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->post(route('admin.logout'))
            ->assertRedirect(route('admin.login'));
    }

    /** @test */
    public function admin_login_redirects_guest_middleware()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.login'))
            ->assertRedirect(route('admin.dashboard'));
    }

    /** @test */
    public function admin_login_post_route_can_redirect_to_dashboard_test()
    {
        $password = 'phpunittest';

        $this->createAdminUser(['is_super_admin' => 1, 'password' => $password])
            ->actingAs($this->user, 'admin')
            ->post(route('admin.login.post', ['email' => $this->user->email, 'password' => $password]))
            ->assertRedirect(route('admin.dashboard'));
    }

    /** @test */
    public function admin_login_post_route_can_fail_test()
    {
        $password = 'phpunittest';

        $this->createAdminUser(['is_super_admin' => 1, 'password' => $password])
            ->post(route('admin.login.post', [
                'email' => $this->user->email,
                'password' => 'wrongpassword'
            ]))
            ->assertSessionHasErrors('email');
    }

    /** @test */
    public function guest_user_is_redirected_to_login_test()
    {
        $this->get(route('admin.dashboard'))
            ->assertRedirect(route('admin.login'));
    }
}
