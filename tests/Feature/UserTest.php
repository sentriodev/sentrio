<?php

namespace Sentrio\Tests\Feature;

use Sentrio\Tests\TestCase;
use Sentrio\User\Controllers\LoginController;

class UserTest extends TestCase
{
    /** @test */
    public function redirect_path_for_login_controller_test()
    {
        $loginController = app(LoginController::class);
        $adminPath = route('admin.dashboard');

        $this->assertEquals($loginController->redirectPath(), $adminPath);
    }
}
