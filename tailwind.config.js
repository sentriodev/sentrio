module.exports = {
  theme: {
    extend: {
      fontFamily: {
        'nunito': ['Nunito', 'sans-serif']
      },
      colors: {
        "modal-700": "rgba(0, 0, 0, 0.70)"
      }
    }
  },
  variants: {},
  plugins: []
}
