<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('meta_title', 'Sentrio eCommerce')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        @if (file_exists(public_path('mix-manifest.json')))
            <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        @else
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @endif
    </head>
    <body>
        <div id="app">

            <base-alert></base-alert>
            <base-dialog></base-dialog>
            <app-layout inline-template>

                <div class="flex items-start">
                    <div :class="sidebar ? 'w-16 z-0 transition sidebar-collapsed duration-500' : 'w-64'">
                        @include('sentrio::partials.sidebar')
                    </div>

                    <div class="w-full">
                        <div class="w-full">
                            @include('sentrio::partials.header')
                            @include('sentrio::partials.flash')
                            @include('sentrio::partials.breadcrumb')

                            <h1 class="mx-4 my-3">
                                @yield('page_title')
                            </h1>

                            <div class="rounded p-5 mx-3 my-3 bg-white">
                                @yield('content')
                            </div>

                            @include('sentrio::partials.footer')
                        </div>
                    </div>
                </div>
            </app-layout>
        </div>

        <!-- Scripts -->
        @if (file_exists(public_path('mix-manifest.json')))
            <script src="{{ mix('js/sentrio.js') }}"></script>
        @else
            <script src="{{ asset('js/sentrio.js') }}"></script>
        @endif

        @stack('scripts')

        @if (file_exists(public_path('mix-manifest.json')))
            <script src="{{ mix('js/app.js') }}"></script>
        @else
            <script src="{{ asset('js/app.js') }}"></script>
        @endif
    </body>
</html>
