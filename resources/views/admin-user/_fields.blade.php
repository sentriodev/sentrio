<div class="mt-3 flex w-full">
    <base-input
        label-text="{{ __('sentrio::core.admin-user.first_name') }}"
        field-name="first_name"
        init-value="{{ $adminUser->first_name ?? '' }}"
        error-text="{{ $errors->first('first_name') }}"
    ></base-input>
</div>

<div class="mt-3 flex w-full">
    <base-input
        label-text="{{ __('sentrio::core.admin-user.last_name') }}"
        field-name="last_name"
        init-value="{{ $adminUser->last_name ?? '' }}"
        error-text="{{ $errors->first('last_name') }}"
    ></base-input>
</div>

<div class="mt-3 flex w-full">
    <base-input
        label-text="{{ __('sentrio::core.admin-user.is_super_admin') }}"
        field-name="is_super_admin"
        init-value="{{ $adminUser->is_super_admin ?? '' }}"
        error-text="{{ $errors->first('is_super_admin') }}"
    ></base-input>
</div>

<div class="mt-3 flex w-full">
    <base-input
        label-text="{{ __('sentrio::core.admin-user.email') }}"
        field-name="email"
        init-value="{{ $adminUser->email ?? '' }}"
        error-text="{{ $errors->first('email') }}"
        :is-disabled="true"
    ></base-input>
</div>

<div class="mt-3 flex w-full">
    <base-input
        label-text="{{ __('sentrio::core.admin-user.image_file') }}"
        field-name="image_path"
        init-value="{{ $adminUser->image_path ?? '' }}"
        error-text="{{ $errors->first('image_path') }}"
        upload-url="{{ route('admin.admin-user-image-upload') }}"
    ></base-input>
</div>

@if (!isset($adminUser))
    <div class="mt-3 flex w-full">
        <base-input
            label-text="{{ __('sentrio::core.admin-user.password') }}"
            field-name="password"
            init-value="{{ $adminUser->password ?? '' }}"
            error-text="{{ $errors->first('password') }}"
            type="password"
        ></base-input>
    </div>

    <div class="mt-3 flex w-full">
        <base-input
            label-text="{{ __('sentrio::core.admin-user.password_confirmation') }}"
            field-name="password_confirmation"
            init-value="{{ $adminUser->password_confirmation ?? '' }}"
            error-text="{{ $errors->first('password_confirmation') }}"
            type="password_confirmation"
        ></base-input>
    </div>
@endif

<div class="mt-3 flex w-full">
    <base-select
        label-text="{{ __('sentrio::core.admin-user.language') }}"
        field-name="language"
        init-value="{{ $adminUser->language ?? '' }}"
        error-text="{{ $errors->first('language') }}"
        :options="{{ json_encode($languageOptions) }}"
    ></base-select>
</div>

<div class="mt-3 flex w-full">
    <base-select
        label-text="{{ __('sentrio::core.admin-user.role_id') }}"
        field-name="role_id"
        init-value="{{ $adminUser->role_id ?? '' }}"
        error-text="{{ $errors->first('role_id') }}"
        :options="{{ json_encode($roleOptions) }}"
    ></base-select>
</div>
