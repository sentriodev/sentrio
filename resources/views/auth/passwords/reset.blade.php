<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('meta_title', 'Sentrio eCommerce')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Scripts -->
        <script defer src="{{ asset('js/app.js') }}"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <div>
                <div class="min-h-screen flex items-center justify-center bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                    <div class="max-w-md w-full bg-white rounded-md shadow-md p-6">
                        <div>
                            <a href="#" target="_blank">
                                <img class="mx-auto h-12 w-auto" src="{{ asset('/img/logo.png') }}" alt="Sentrio eCommerce" />
                            </a>

                            <h2 class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
                                {{ __('Set New Password') }}
                            </h2>

                            <p class="mt-2 text-center text-sm leading-5 text-gray-600 max-w"></p>
                        </div>

                        <form class="mt-8" action="{{ route('admin.password.update') }}" method="POST">
                            @csrf()
                            <input type="hidden" name="remember" value="true" />

                            <div class="rounded-md shadow-sm">
                                <div class="mt-3">
                                    <base-input></base-input>
                                </div>
                            </div>

                            <div class="rounded-md shadow-sm">
                                <div class="mt-3">
                                    <base-input></base-input>
                                </div>
                            </div>

                            <div class="rounded-md shadow-sm">
                                <div class="mt-3">
                                    <base-input></base-input>
                                </div>
                            </div>

                            <input type="hidden" name="token" value="{{ $token }}" />

                            <div class="mt-6">
                                <button type="submit" class="w-full flex justify-center py-2 px-4 border border-red-500 text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red active:bg-red-700">
                                    <span class="absolute left-0 inset-y pl-3">
                                        <svg class="h-5 w-5 text-red-800" fill="currentColor" viewBox="0 0 20 20">
                                            <path fill-rule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clip-rule="evenodd" />
                                        </svg>
                                    </span>

                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @stack('scripts')
    </body>
</html>
