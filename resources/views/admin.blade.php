@extends('sentrio::layouts.app')

@section('meta_title')
    Sentrio eCommerce Admin Dashboard
@endsection

@section('page_title')
    <div class="text-gray-800 flex items-center">
        <div class="text-xl text-red-700 font-semibold">
            {{ __('Dashboard') }}
        </div>
    </div>
@endsection

@section('content')
    <div class="flex justify-around my-5">
        {{ $customerWidget->render() }}
        {{ $orderWidget->render() }}
        {{ $revenueWidget->render() }}
    </div>

    <div class="flex mt-5 justify-around my-5">
        <div class="bg-white border border-gray-400 w-full rounded">
            <div class="font-semibold text-md p-4 border-b">
                Admin Dashboard
            </div>
        </div>
    </div>
@endsection
