<?php

return [

    'out_of_stock' => 'Sorry, this product is out of stock.',
    'in_stock' => ':qty available in stock.'
];
