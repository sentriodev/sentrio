<?php

return [

    'fields' => [],

    'dashboard' => 'Dashboard',

    'total-customer' => 'Total Customer',
    'total-order' => 'Total Order',
    'total-revenue' => 'Total Revenue',

    'notification' => [
        'store' => ':attribute created successfully!',
        'update' => ':attribute updated successfully!',
        'delete' => ':attribute deleted successfully!',
        'save' => ':attribute saved successfully!',
        'upload' => ':attribute uploaded successfully!',
        'approved' => ':attribute approved successfully!',
        'insufficient_qty' => 'Sorry, we do not have enough quantity available for this product.',
    ],

    'admin-user' => [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Confirm Password',
        'is_super_admin' => 'Is Administrator',
        'role_id' => 'Role',
        'language' => 'Language',
        'image_file' => 'Admin User Profile Image',
        'index' => 'Admin User List',
        'create' => 'Admin User Create',
        'edit' => 'Admin User Edit'
    ],

    'btn' => [
        'save' => 'Save',
        'cancel' => 'Cancel',
        'create' => 'Create'
    ],

];
