<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'form' => [
        'title' => 'Login to Admin Account',
        'email' => 'Enter Email',
        'password' => 'Enter Password',
        'password_confirmation' => 'Confirm Password',
        'forgot_password' => 'Forgot Password?',
        'remember_me' => 'Remember Me',
        'login' => 'Login'
    ],

    'reset' => [
        'title' => 'Reset Password',
        'reset_btn' => 'Password Reset Link Sent'
    ],

    'password' => [
        'title' => 'Change New Password',
        'submit_btn' => 'Change Password'
    ],

];
