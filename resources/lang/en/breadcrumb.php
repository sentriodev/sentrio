<?php

return [

    'dashboard' => 'Dashboard',
    'category' => [
        'index' => 'Category',
        'edit' => 'Edit Category',
        'create' => 'Create Category'
    ],

    'product' => [
        'index' => 'Product',
        'edit' => 'Edit Product',
        'create' => 'Create Product'
    ],

    'order' => [
        'index' => 'Order',
        'show' => 'Show Order'
    ],

    'menu' => [
        'index' => 'Menu',
        'edit' => 'Edit Menu',
        'create' => 'Create Menu'
    ],

    'tax-group' => [
        'index' => 'Tax Group',
        'edit' => 'Edit Tax Group',
        'create' => 'Create Tax Group'
    ],

    'tax-rate' => [
        'index' => 'Tax Rate',
        'edit' => 'Edit Rate',
        'create' => 'Create Rate'
    ],

    'user-group' => [
        'index' => 'User Group',
        'edit' => 'Edit User Group',
        'create' => 'Create User Group'
    ],

];
