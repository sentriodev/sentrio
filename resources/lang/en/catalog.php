<?php

return [

    'category' => [
        'title' => 'Category',
        'name' => 'Name',
        'slug' => 'Slug',
        'meta_title' => 'Meta Title',
        'meta_description' => 'Meta Description',
        'index' => [
            'title' => 'Category List'
        ],
        'create' => [
            'title' => 'Category Create'
        ],
        'edit' => [
            'title' => 'Category Edit'
        ]
    ],

    'product' => [
        'title' => 'Product',
        'name' => 'Name',
        'slug' => 'Slug',
        'qty' => 'Quantity',
        'is_taxable' => 'Is Taxable',
        'track_stock' => 'Track Stock',
        'weight' => 'Weight',
        'height' => 'Height',
        'length' => 'Length',
        'category' => 'Category',
        'in_stock' => ':qty Available In Stock',
        'out_of_stock' => 'Sorry, this product is out of stock.',
        'price' => 'Price',
        'cost_price' => 'Cost Price',
        'type' => 'Product Type',
        'sku' => 'SKU',
        'barcode' => 'Barcode',
        'description' => 'Description',
        'meta_title' => 'Meta Title',
        'meta_description' => 'Meta Description',
        'image_title' => 'Product Image',
        'upload_btn' => 'Upload',
        'index' => [
            'title' => 'Product List'
        ],
        'create' => [
            'title' => 'Product Create'
        ],
        'edit' => [
            'title' => 'Product Edit'
        ]
    ],

    'cart_success' => 'Product Added to Cart',

];
