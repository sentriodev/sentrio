import BaseDialog from './BaseDialog.vue'

export default {
    install(Vue, args = {}) {
        if (this.installed) return

        this.installed = true
        this.params = args

        Vue.component('base-dialog', BaseDialog)

        const dialog = params => {
            window.EventBus.$emit("dialogOpen", params)
        }

        Vue.prototype.$dialog = dialog
        Vue['$dialog'] = dialog
    }
}
