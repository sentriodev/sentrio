import './components/base/index';

import {
    BaseInput,
    BaseTable,
    BaseUpload,
    BaseSelect,
    BaseToggle,
    BaseTabs,
    BaseTab,
    BaseModal,
    BaseAlert,
    BaseDialog
} from "base";

Vue.component("base-table", BaseTable);
Vue.component("base-input", BaseInput);
Vue.component("base-upload", BaseUpload);
Vue.component("base-select", BaseSelect);
Vue.component("base-toggle", BaseToggle);
Vue.component('base-tabs', BaseTabs);
Vue.component('base-tab', BaseTab);
Vue.component("base-modal", BaseModal);

Vue.component('base-menu', require('./components/Menu.vue'))

Vue.use(BaseAlert);
Vue.use(BaseDialog);
