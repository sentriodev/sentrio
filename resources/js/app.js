window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
    console.error("CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token");
}

import Router from './router'
import Store from './store'
import './base'

import Vddl from 'vddl'
import Zondicon from 'vue-zondicons'
import Vue from 'vue';

Vue.use(Vddl)

Vue.component("zondicon", Zondicon);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('app-layout', require('./components/Layout.vue').default)
Vue.component('app-flash', require('./components/Flash.vue').default)
Vue.component('app-login', require('./components/Login.vue').default)
Vue.component('password-forgot', require('./components/PasswordForgot.vue').default)
Vue.component('password-new', require('./components/PasswordNew.vue').default)

const app = new Vue({
    el: "#app",
    router: Router,
    store: Store
});
