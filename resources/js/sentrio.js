window.Vue = require('vue');

window.Sentrio = (function () {
    return {
        initialize: function (callback) {
            callback(window.Vue)
        }
    };
})();

window.EventBus = new Vue();

exports = module.exports = Sentrio;
