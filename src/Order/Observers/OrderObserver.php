<?php

namespace Sentrio\Order\Observers;

use Sentrio\Database\Models\Order;

class OrderObserver
{
    /**
     * Handle the Order "created" event
     *
     * @param  \Sentrio\Database\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }
}
