<?php

namespace Sentrio\Order\Events;

use Illuminate\Queue\SerializesModels;
use Sentrio\Database\Models\OrderProduct;

class OrderProductCreated
{
    use SerializesModels;

    /**
     * The order product model
     *
     * @var \Sentrio\Database\Models\OrderProduct
     */
    public $orderProduct;

    /**
     * Create a new event instance
     *
     * @param  \Sentrio\Database\Models\OrderProduct $orderProduct
     * @return void
     */
    public function __construct(OrderProduct $orderProduct)
    {
        $this->orderProduct = $orderProduct;
    }
}
