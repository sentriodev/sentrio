<?php

namespace Sentrio\Order\Listeners;

use Sentrio\Order\Events\OrderProductCreated;

class OrderProductCreatedListener
{
    /**
     * Handle the event.
     *
     * @param  \Sentrio\Order\Events\OrderProductCreated $event
     * @return void
     */
    public function handle(OrderProductCreated $event)
    {
        $orderProduct = $event->orderProduct;

        $product = $orderProduct->product;
        $product->qty -= $orderProduct->qty;
        $product->save();
    }
}
