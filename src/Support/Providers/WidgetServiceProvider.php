<?php

namespace Sentrio\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Sentrio\Support\Facades\Widget;
use Sentrio\Widgets\Concerns\TotalCustomer;
use Sentrio\Widgets\Concerns\TotalOrder;
use Sentrio\Widgets\Concerns\TotalRevenue;
use Sentrio\Widgets\WidgetManager;

class WidgetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if the loading of providers is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot()
    {
        $this->bootWidgets();
    }

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->alias('widget', 'Sentrio\Widgets\WidgetManager');
    }

    /**
     * Get the services provided by the provider
     *
     * @return array
     */
    public function provides()
    {
        return ['widget', 'Sentrio\Widgets\WidgetManager'];
    }

    /**
     * Register the widget service
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('widget', function ($app) {
            return new WidgetManager();
        });
    }

    /**
     * Bootstrap the widgets
     *
     * @return void
     */
    protected function bootWidgets()
    {
        $totalCustomer = new TotalCustomer;
        $totalOrder = new TotalOrder;
        $totalRevenue = new TotalRevenue;

        Widget::make($totalCustomer->identifier(), $totalCustomer);
        Widget::make($totalOrder->identifier(), $totalOrder);
        Widget::make($totalRevenue->identifier(), $totalRevenue);
    }
}
