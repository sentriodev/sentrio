<?php

namespace Sentrio\Support\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Sentrio\Permissions\Manager;
use Sentrio\Permissions\Permission;
use Sentrio\Permissions\PermissionGroup;
use Sentrio\Support\Facades\Permission as PermissionFacade;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->singleton('permission', 'Sentrio\Permissions\Manager');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootPermissions();
    }

    /**
     * Get the services provided by the provider
     *
     * @return array
     */
    public function provides()
    {
        return ['permission', 'Sentrio\Permissions\Manager'];
    }

    /**
     * Register the permission manager service
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('permission', function () {
            new Manager();
        });
    }

    protected function bootPermissions()
    {
        $group = PermissionFacade::add('dashboard', function (PermissionGroup $group) {
            $group->label('sentrio::permissions.dashboard');
        });
        $group->addPermission('admin-dashboard', function (Permission $permission) {
            $permission
                ->label('sentrio::permissions.dashboard')
                ->routes('admin-dashboard');
        });
    }
}
