<?php

namespace Sentrio\Support\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Sentrio\Database\Models\Order;
use Sentrio\Order\Events\OrderProductCreated;
use Sentrio\Order\Listeners\OrderProductCreatedListener;
use Sentrio\Order\Observers\OrderObserver;
use Sentrio\User\Observers\UserObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event handler mappings for the application
     *
     * @var array
     */
    protected $listen = [
        OrderProductCreated::class => [
            OrderProductCreatedListener::class
        ]
    ];

    /**
     * Boot any application services
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->bootUserModelObserver();
    }

    /**
     * Bootstrap the user model observer
     *
     * @return void
     */
    public function bootUserModelObserver()
    {
        $user = config('sentrio.user.model');

        try {
            $model = app($user);
        } catch (\Exception $e) {
            $model = null;
        }

        if ($model !== null) {
            $model->observe(UserObserver::class);
        }

        Order::observe(OrderObserver::class);
    }
}
