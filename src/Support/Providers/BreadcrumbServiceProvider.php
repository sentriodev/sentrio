<?php

namespace Sentrio\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Sentrio\Breadcrumbs\Breadcrumb;
use Sentrio\Breadcrumbs\Builder;
use Sentrio\Support\Facades\Breadcrumb as BreadcrumbFacade;

class BreadcrumbServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->alias('breadcrumb', 'Sentrio\Breadcrumbs\Builder');
    }

    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot()
    {
        $this->bootBreadcrumbs();
    }

    /**
     * Get the services provided by the provider
     *
     * @return array
     */
    public function provides()
    {
        return ['breadcrumb', 'Sentrio\Breadcrumbs\Builder'];
    }

    /**
     * Register the breadcrumb builder
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('breadcrumb', function () {
            return new Builder();
        });
    }

    protected function bootBreadcrumbs()
    {
        BreadcrumbFacade::make('admin.dashboard', function (Breadcrumb $breadcrumb) {
            $breadcrumb->label('sentrio::breadcrumb.dashboard');
        });
    }
}
