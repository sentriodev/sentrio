<?php

namespace Sentrio\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Sentrio\Support\Traits\ModelBindings;

class RepositoryServiceProvider extends ServiceProvider
{
    use ModelBindings;

    /**
     * Indicates if loading of the provider is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->bindings as $interface => $repository) {
            $this->app->bind($interface, $repository);
        }
    }
}
