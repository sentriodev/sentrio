<?php

namespace Sentrio\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Sentrio\Menus\MenuBuilder;
use Sentrio\Menus\MenuItem;
use Sentrio\Support\Facades\Menu;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->alias('menu', 'Sentrio\Menus\MenuBuilder');
    }

    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Get the services provided by the provider
     *
     * @return array
     */
    public function provides()
    {
        return ['menu', 'Sentrio\Menus\MenuBuilder'];
    }

    /**
     * Register the service
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('menu', function () {
            return new MenuBuilder();
        });
    }

    protected function bootMenu()
    {
        Menu::make('user', function (MenuItem $menu) {
            $menu->label('User')
                ->type(MenuItem::IS_ADMIN)
                ->icon('user-group')
                ->route('#');
        });

        $userMenu = Menu::get('user');
        $userMenu->subMenu('admin-user', function (MenuItem $menu) {
            $menu->key('admin-user')
                ->type(MenuItem::IS_ADMIN)
                ->label('Admin User')
                ->route('admin.admin-user.index');
        });
        $userMenu->subMenu('user_group', function (MenuItem $menu) {
            $menu->key('user_group')
                ->type(MenuItem::IS_ADMIN)
                ->label('User Group')
                ->route('admin.user-group.index');
        });
    }
}
