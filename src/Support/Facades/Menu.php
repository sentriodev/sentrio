<?php

namespace Sentrio\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed label(string $label = null)
 * @method static mixed type(string $type = null)
 * @method static mixed key(string $key = null)
 * @method static mixed route(string $routeName = null)
 * @method static mixed parameters(string $parameters = null)
 * @method static mixed icon(string $icon = null)
 * @method static mixed attributes(string $attributes = null)
 * @method static \Sentrio\Menus\MenuItem subMenu(string $key = null, $item = null)
 * @method static bool hasSubMenu()
 * @method static \Sentrio\Menus\MenuBuilder make(string $key, callable $callable)
 * @method static mixed get(string $key)
 * @method static \Illuminate\Support\Collection all(bool $admin = false)
 * @method static array getMenuFromRouteName(string $name)
 * @method static \Illuminate\Support\Collection frontMenus()
 * @method static \Illuminate\Support\Collection adminMenus()
 */
class Menu extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'menu';
    }
}
