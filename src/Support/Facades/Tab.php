<?php

namespace Sentrio\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed label(string $label = null)
 * @method static mixed view(string $view = null)
 * @method static mixed key(string $key = null)
 * @method static \Illuminate\Support\Collection all()
 * @method static \Illuminate\Support\Collection get(string $key)
 * @method static \Sentrio\Tabs\TabManager put(string $key, callable $tab)
 */
class Tab extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'tab';
    }
}
