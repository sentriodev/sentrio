<?php

namespace Sentrio\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed label($label = null)
 * @method static mixed key($key = null)
 * @method static \Sentrio\Permissions\Permission addPermission(string $key, $callable = null)
 * @method static \Sentrio\Permissions\PermissionGroup add($key, $callable = null)
 * @method static string routes($routes = null)
 * @method static \Illuminate\Support\Collection get(string $key)
 * @method static \Sentrio\Permissions\Manager set(string $key, mixed $permission)
 * @method static \Illuminate\Support\Collection all()
 */
class Permission extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'permission';
    }
}
