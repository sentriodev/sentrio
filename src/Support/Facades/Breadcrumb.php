<?php

namespace Sentrio\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed label(string $key = null)
 * @method static mixed route(string $route = null)
 * @method static \Sentrio\Breadcrumbs\Breadcrumb parent(string $key)
 * @method static void make(string $name, callable $callable)
 * @method static mixed get(string $key)
 * @method static string|\Illuminate\View\View render(string $routeName)
 */
class Breadcrumb extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'breadcrumb';
    }
}
