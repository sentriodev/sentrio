<?php

namespace Sentrio\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed label(string $label = null)
 * @method static mixed type(string $type = null)
 * @method static \Sentrio\Widgets\Widget make(string $key, \Sentrio\Widgets\Widget $widget)
 * @method static mixed get(string $key)
 * @method static \Illuminate\Support\Collection all()
 * @method static \Illuminate\Support\Collection options()
 */
class Widget extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'widget';
    }
}
