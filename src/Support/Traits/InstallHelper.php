<?php

namespace Sentrio\Support\Traits;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

trait InstallHelper
{
    /**
     * The role repository instance
     *
     * @var \Sentrio\Database\Contracts\RoleInterface
     */
    protected $roleRepository;

    /**
     * The order status repository instance
     *
     * @var \Sentrio\Database\Contracts\OrderStatusInterface
     */
    protected $orderStatusRepository;

    /**
     * The user group repository instance
     *
     * @var \Sentrio\Database\Contracts\UserGroupInterface
     */
    protected $userGroupRepository;

    /**
     * The currency repository instance
     *
     * @var \Sentrio\Database\Contracts\CurrencyInterface
     */
    protected $currencyRepository;

    /**
     * The language repository instance
     *
     * @var \Sentrio\Database\Contracts\LanguageInterface
     */
    protected $languageRepository;

    /**
     * The admin user repository instance
     *
     * @var \Sentrio\Database\Contracts\AdminUserInterface
     */
    protected $adminUserRepository;

    /**
     * Bootstrap the default currency
     *
     * @return void
     */
    public function bootCurrency()
    {
        $this->currencyRepository->create([
            'name' => 'US Dollar',
            'code' => 'usd',
            'symbol' => '$',
            'conversion_rate' => 1,
            'status' => 'ENABLED'
        ]);
    }

    /**
     * Bootstrap the default user group
     *
     * @return void
     */
    public function bootDefaultUserGroup()
    {
        $this->userGroupRepository->create([
            'name' => 'Default Group',
            'is_default' => 1
        ]);
    }

    /**
     * Bootstrap the default order status
     *
     * @return void
     */
    public function bootOrderStatus()
    {
        $default = $this->orderStatusRepository->create(['name' => 'Pending']);
        $default->is_default = 1;
        $default->save();

        $this->orderStatusRepository->create(['name' => 'Processing']);
        $this->orderStatusRepository->create(['name' => 'Completed']);
    }

    /**
     * Bootstrap the default language
     *
     * @return void
     */
    public function bootLanguage()
    {
        $this->languageRepository->create([
            'name' => 'English',
            'code' => 'en',
            'is_default' => 1
        ]);
    }

    /**
     * Alter the user table
     *
     * @return void
     */
    public function alterUserTable()
    {
        $user = config('sentrio.user.model');

        try {
            $model = resolve($user);
        } catch (\Exception $e) {
            $model = null;
        }

        if ($model !== null) {
            $table = $model->getTable();

            if (Schema::hasTable($table)) {
                Schema::table($table, function (Blueprint $table) {
                    $table->unsignedBigInteger('user_group_id')->nullable()->default(null);
                });
            }
        }
    }
}
