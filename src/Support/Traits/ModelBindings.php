<?php

namespace Sentrio\Support\Traits;

use Sentrio\Database\Contracts\AddressInterface;
use Sentrio\Database\Contracts\AdminUserInterface;
use Sentrio\Database\Contracts\AttributeInterface;
use Sentrio\Database\Contracts\CategoryFilterInterface;
use Sentrio\Database\Contracts\CategoryInterface;
use Sentrio\Database\Contracts\CountryInterface;
use Sentrio\Database\Contracts\CurrencyInterface;
use Sentrio\Database\Contracts\CustomerInterface;
use Sentrio\Database\Contracts\LanguageInterface;
use Sentrio\Database\Contracts\MenuGroupInterface;
use Sentrio\Database\Contracts\MenuInterface;
use Sentrio\Database\Contracts\OrderInterface;
use Sentrio\Database\Contracts\OrderProductInterface;
use Sentrio\Database\Contracts\OrderStatusInterface;
use Sentrio\Database\Contracts\PermissionInterface;
use Sentrio\Database\Contracts\ProductImageInterface;
use Sentrio\Database\Contracts\ProductInterface;
use Sentrio\Database\Contracts\PropertyInterface;
use Sentrio\Database\Contracts\RoleInterface;
use Sentrio\Database\Contracts\StateInterface;
use Sentrio\Database\Contracts\TaxGroupInterface;
use Sentrio\Database\Contracts\TaxRateInterface;
use Sentrio\Database\Contracts\UserGroupInterface;
use Sentrio\Database\Repositories\AddressRepository;
use Sentrio\Database\Repositories\AdminUserRepository;
use Sentrio\Database\Repositories\AttributeRepository;
use Sentrio\Database\Repositories\CategoryFilterRepository;
use Sentrio\Database\Repositories\CategoryRepository;
use Sentrio\Database\Repositories\CountryRepository;
use Sentrio\Database\Repositories\CurrencyRepository;
use Sentrio\Database\Repositories\CustomerRepository;
use Sentrio\Database\Repositories\LanguageRepository;
use Sentrio\Database\Repositories\MenuGroupRepository;
use Sentrio\Database\Repositories\MenuRepository;
use Sentrio\Database\Repositories\OrderProductRepository;
use Sentrio\Database\Repositories\OrderRepository;
use Sentrio\Database\Repositories\OrderStatusRepository;
use Sentrio\Database\Repositories\PermissionRepository;
use Sentrio\Database\Repositories\ProductImageRepository;
use Sentrio\Database\Repositories\ProductRepository;
use Sentrio\Database\Repositories\PropertyRepository;
use Sentrio\Database\Repositories\RoleRepository;
use Sentrio\Database\Repositories\StateRepository;
use Sentrio\Database\Repositories\TaxGroupRepository;
use Sentrio\Database\Repositories\TaxRateRepository;
use Sentrio\Database\Repositories\UserGroupRepository;

trait ModelBindings
{
    /**
     * The repository bindings
     *
     * @var array
     */
    public $bindings = [
        AddressInterface::class => AddressRepository::class,
        AdminUserInterface::class => AdminUserRepository::class,
        AttributeInterface::class => AttributeRepository::class,
        CategoryFilterInterface::class => CategoryFilterRepository::class,
        CategoryInterface::class => CategoryRepository::class,
        CountryInterface::class => CountryRepository::class,
        CurrencyInterface::class => CurrencyRepository::class,
        CustomerInterface::class => CustomerRepository::class,
        LanguageInterface::class => LanguageRepository::class,
        MenuGroupInterface::class => MenuGroupRepository::class,
        MenuInterface::class => MenuRepository::class,
        OrderInterface::class => OrderRepository::class,
        OrderProductInterface::class => OrderProductRepository::class,
        OrderStatusInterface::class => OrderStatusRepository::class,
        PermissionInterface::class => PermissionRepository::class,
        ProductImageInterface::class => ProductImageRepository::class,
        ProductInterface::class => ProductRepository::class,
        PropertyInterface::class => PropertyRepository::class,
        RoleInterface::class => RoleRepository::class,
        StateInterface::class => StateRepository::class,
        TaxGroupInterface::class => TaxGroupRepository::class,
        TaxRateInterface::class => TaxRateRepository::class,
        UserGroupInterface::class => UserGroupRepository::class
    ];
}
