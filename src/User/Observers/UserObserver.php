<?php

namespace Sentrio\User\Observers;

use Sentrio\Database\Contracts\UserGroupInterface;

class UserObserver
{
    /**
     * The user group repository instance
     *
     * @var \Sentrio\Database\Contracts\UserGroupInterface
     */
    protected $userGroupRepository;

    /**
     * Create a new observer instance
     *
     * @param  \Sentrio\Database\Contracts\UserGroupInterface $userGroupRepository
     * @return void
     */
    public function __construct(UserGroupInterface $userGroupRepository)
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Handle the User "created" event
     *
     * @param  mixed $user
     * @return void
     */
    public function created($user)
    {
        $userGroup = $this->userGroupRepository->getIsDefault();

        $user->user_group_id = $userGroup->id;
        $user->save();
    }
}
