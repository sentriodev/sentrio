<?php

namespace Sentrio\User\Controllers;

use Sentrio\Database\Contracts\UserGroupInterface;
use Sentrio\Database\Models\UserGroup;
use Sentrio\User\Requests\UserGroupRequest;

class UserGroupController
{
    protected $userGroupRepository;

    public function __construct(UserGroupInterface $userGroupRepository)
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(UserGroupRequest $request)
    {
        //
    }

    public function edit(UserGroup $userGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage
     *
     * @param  \Sentrio\User\Requests\UserGroupRequest $request
     * @param  \Sentrio\Database\Models\UserGroup      $userGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserGroupRequest $request, UserGroup $userGroup)
    {
        if ($request->get('is_default')) {
            $group = $this->userGroupRepository->getIsDefault();
            $group->update(['is_default' => 0]);
        }

        $userGroup->update($request->all());

        return redirect()
            ->route('admin.user-group.index')
            ->with('successNotification', __('sentrio::core.notification.update', ['attribute' => 'User Group']));
    }

    /**
     * Remove the specified resource from storage
     *
     * @param  \Sentrio\Database\Models\UserGroup $userGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserGroup $userGroup)
    {
        $userGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __('sentrio::core.notification.delete', ['attribute' => 'User Group'])
        ]);
    }
}
