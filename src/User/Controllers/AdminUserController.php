<?php

namespace Sentrio\User\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Sentrio\Database\Contracts\AdminUserInterface;
use Sentrio\Database\Contracts\RoleInterface;
use Sentrio\Database\Models\AdminUser;
use Sentrio\Support\Facades\Tab;
use Sentrio\User\Requests\AdminUserImageRequest;
use Sentrio\User\Requests\AdminUserRequest;

class AdminUserController extends Controller
{
    /**
     * The admin user repository instance
     *
     * @var \Sentrio\Database\Contracts\AdminUserInterface
     */
    protected $adminUserRepository;

    /**
     * The role repository instance
     *
     * @var \Sentrio\Database\Contracts\RoleInterface
     */
    protected $roleRepository;

    /**
     * Create a new controller instance
     *
     * @param  \Sentrio\Database\Contracts\AdminUserInterface $adminUserRepository
     * @param  \Sentrio\Database\Contracts\RoleInterface  $roleRepository
     * @return void
     */
    public function __construct(AdminUserInterface $adminUserRepository, RoleInterface $roleRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adminUsers = $this->adminUserRepository->paginate();

        return view('sentrio::admin-user.index')
            ->with(compact('adminUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('admin-user');
        $languageOptions = ['en' => 'English'];
        $roleOptions = $this->roleRepository->options();

        return view('sentrio::admin-user.create')
            ->with(compact('roleOptions', 'tabs', 'languageOptions'));
    }

    /**
     * Store a newly created resource in storage
     *
     * @param  \Sentrio\User\Requests\AdminUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);

        $this->adminUserRepository->create($request->all());

        return redirect()
            ->route('admin.admin-user.index')
            ->with('successNotification', __('sentrio::core.notification.store', ['attribute' => 'Admin User']));
    }

    /**
     * Show the form for editing the specified resource
     *
     * @param  \Sentrio\Database\Models\AdminUser $adminUser
     * @return \Illuminate\View\View
     */
    public function edit(AdminUser $adminUser)
    {
        $tabs = Tab::get('admin-user');
        $languageOptions = ['en' => 'English'];
        $roleOptions = $this->roleRepository->options();

        return view('sentrio::admin-user.edit')
            ->with(compact('adminUser', 'roleOptions', 'tabs', 'languageOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Sentrio\User\Requests\AdminUserRequest  $request
     * @param  \Sentrio\Database\Models\AdminUser $adminUser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserRequest $request, AdminUser $adminUser)
    {
        $adminUser->update($request->all());

        return redirect()
            ->route('admin.admin-user.index')
            ->with('successNotification', __('sentrio::core.notification.update', ['attribute' => 'Admin User']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sentrio\Database\Models\AdminUser $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(AdminUser $adminUser)
    {
        $adminUser->delete();

        return response()->json([
            'success' => true,
            'message' => __('sentrio::core.notification.delete', ['attribute' => 'Admin User'])
        ]);
    }

    /**
     * Upload a specified image resource in storage
     *
     * @param  \Sentrio\User\Requests\AdminUserImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(AdminUserImageRequest $request)
    {
        $image = $request->file('files');
        $path = $image->store('uploads/users', 'sentrio');

        return response()->json([
            'success' => true,
            'path' => $path,
            'message' => __('sentrio::core.notification.upload', ['attribute' => 'Admin User Image'])
        ]);
    }
}
