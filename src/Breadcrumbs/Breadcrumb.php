<?php

namespace Sentrio\Breadcrumbs;

use Illuminate\Support\Collection;
use Sentrio\Contracts\BreadcrumbContract;
use Sentrio\Support\Facades\Breadcrumb as BreadcrumbFacade;

class Breadcrumb implements BreadcrumbContract
{
    /**
     * The breadcrumb label
     *
     * @var string|null
     */
    public $label = null;

    /**
     * The breadcrumb route
     *
     * @var string|null
     */
    public $route = null;

    /**
     * The collection of breadcrumb parents
     *
     * @var \Illuminate\Support\Collection
     */
    public $parents = null;

    /**
     * Create a new breadcrumb instance
     *
     * @param  callable $callable
     */
    public function __construct($callable)
    {
        $this->parents = new Collection();

        $callable($this);
    }

    /**
     * Return the breadcrumb label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null === $label) {
            return $this->label;
        }

        $this->label = $label;

        return $this;
    }

    /**
     * Return the breadcrumb route
     *
     * @param  string|null $route
     * @return mixed
     */
    public function route($route = null)
    {
        if (null === $route) {
            return $this->route;
        }

        $this->route = $route;

        return $this;
    }

    /**
     * Set the breadcrumb parent
     *
     * @param  string $key
     * @return $this
     */
    public function parent($key): self
    {
        $breadcrumb = BreadcrumbFacade::get($key);

        $this->parents->put($key, $breadcrumb);

        return $this;
    }
}
