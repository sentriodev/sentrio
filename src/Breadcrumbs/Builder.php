<?php

namespace Sentrio\Breadcrumbs;

use Illuminate\Support\Collection;

class Builder
{
    /**
     * The breadcrumb instance
     *
     * @var \Sentrio\Breadcrumbs\Breadcrumb
     */
    protected $breadcrumb;

    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create a new breadcrumb builder instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = new Collection();
    }

    /**
     * Create a new breadcrumb collection
     *
     * @param  string   $name
     * @param  callable $callable
     * @return void
     */
    public function make($name, callable $callable)
    {
        $breadcrumb = new Breadcrumb($callable);
        $breadcrumb->route($name);

        $this->collection->put($name, $breadcrumb);
    }

    /**
     * Get an item from the collection by key
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->collection->get($key);
    }

    /**
     * Render the breadcrumb by the given route name
     *
     * @param  string $routeName
     * @return string|\Illuminate\View\View
     */
    public function render($routeName)
    {
        $breadcrumb = $this->collection->get($routeName);
        if (null === $breadcrumb) {
            return '';
        }

        return view('sentrio::breadcrumb.index')->with(compact('breadcrumb'));
    }
}
