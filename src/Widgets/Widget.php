<?php

namespace Sentrio\Widgets;

use Sentrio\Contracts\WidgetContract;

class Widget implements WidgetContract
{
    /**
     * The widget label
     *
     * @var string|null
     */
    public $label = null;

    /**
     * The widget type
     *
     * @var string|null
     */
    public $type = null;

    /**
     * The widget callback
     *
     * @var callable|null
     */
    protected $callable = null;

    /**
     * Create new widget instance
     *
     * @param  callable|null $callable
     * @return void
     */
    public function __construct($callable)
    {
        $this->callable = $callable;

        $callable($this);
    }

    /**
     * Return the widget label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null === $label) {
            return $this->label;
        }

        $this->label = $label;

        return $this;
    }

    /**
     * Return the widget type
     *
     * @param  string|null $type
     * @return mixed
     */
    public function type($type = null)
    {
        if (null === $type) {
            return $this->type;
        }

        $this->type = $type;

        return $this;
    }
}
