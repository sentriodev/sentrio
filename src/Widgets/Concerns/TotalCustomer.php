<?php

namespace Sentrio\Widgets\Concerns;

use Illuminate\Support\Carbon;

class TotalCustomer
{
    /**
     * The widget view path
     *
     * @var string
     */
    protected $view = 'sentrio::widget.total-customer';

    /**
     * The widget label
     *
     * @var string
     */
    protected $label = 'Total Customer';

    /**
     * The widget type
     *
     * @var string
     */
    protected $type = 'cms';

    /**
     * The widget identifier
     *
     * @var string
     */
    protected $identifier = 'sentrio-total-customer';

    /**
     * Get the widgets view path
     *
     * @return string
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * Get the widgets label
     *
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * Get the widgets type
     *
     * @return string
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Get the widgets identifier
     *
     * @return string
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * Add a piece of data to the view
     *
     * @return mixed
     */
    public function with()
    {
        $userModel = $this->getUserModel();
        $firstDay = $this->getFirstDay();

        if ($userModel === null) {
            $value = 0;
        } else {
            $value = $userModel->select('id')->where('created_at', '>', $firstDay)->count();
        }

        return ['value' => $value];
    }

    /**
     * Get the string contents of the view
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view($this->view(), $this->with());
    }

    /**
     * Get the starting day of the month
     *
     * @return mixed
     */
    protected function getFirstDay()
    {
        $startDay = Carbon::now();

        return $startDay->firstOfMonth();
    }

    /**
     * Get the user model
     *
     * @return mixed
     */
    protected function getUserModel()
    {
        $user = config('sentrio.user.model');

        try {
            $model = app($user);
        } catch (\Exception $e) {
            $model = null;
        }

        return $model;
    }
}
