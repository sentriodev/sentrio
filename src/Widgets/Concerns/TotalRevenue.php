<?php

namespace Sentrio\Widgets\Concerns;

use Sentrio\Database\Contracts\OrderInterface;

class TotalRevenue
{
    /**
     * The widget view path
     *
     * @var string
     */
    protected $view = 'sentrio::widget.total-revenue';

    /**
     * The widget label
     *
     * @var string
     */
    protected $label = 'Total Revenue';

    /**
     * The widget type
     *
     * @var string
     */
    protected $type = 'cms';

    /**
     * The widget identifier
     *
     * @var string
     */
    protected $identifier = 'sentrio-total-revenue';

    /**
     * Get the widgets view path
     *
     * @return string
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * Get the widgets label
     *
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * Get the widgets type
     *
     * @return string
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Get the widgets identifier
     *
     * @return string
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * Add a piece of data to the view
     *
     * @return mixed
     */
    public function with()
    {
        $orderRepository = app(OrderInterface::class);
        $value = $orderRepository->getCurrentMonthTotalRevenue();

        return ['value' => $value];
    }

    /**
     * Get the string contents of the view
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view($this->view(), $this->with());
    }
}
