<?php

namespace Sentrio\Widgets;

use Illuminate\Support\Collection;

class WidgetManager
{
    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create a new widget manager instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    /**
     * Create a widget instance to the collection
     *
     * @param  string $key
     * @param  \Sentrio\Widgets\Widget $widget
     * @return \Sentrio\Widgets\Widget
     */
    public function make($key, $widget)
    {
        $this->collection->put($key, $widget);

        return $widget;
    }

    /**
     * Get a widget from the collection by key
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->collection->get($key);
    }

    /**
     * Returns all of widgets from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->collection;
    }

    /**
     * Returns a new collection instance
     *
     * @return \Illuminate\Support\Collection
     */
    public function options(): Collection
    {
        $options = Collection::make([]);

        foreach ($this->collection as $key => $widget) {
            $options->put($key, $widget->label());
        }

        return $options;
    }
}
