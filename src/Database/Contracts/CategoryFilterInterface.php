<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\CategoryFilter;

interface CategoryFilterInterface
{
    public function create(array $data): CategoryFilter;

    public function find(int $id): CategoryFilter;

    public function delete(int $id): int;

    public function all(): Collection;

    public function findByCategoryId(int $id): Collection;

    public function isCategoryFilterModelExists(int $categoryId, int $filterId, $type);
}
