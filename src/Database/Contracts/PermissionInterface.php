<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\Permission;

interface PermissionInterface
{
    public function create(array $data): Permission;

    public function find(int $id): Permission;

    public function all(): Collection;

    public function findByName(string $name);
}
