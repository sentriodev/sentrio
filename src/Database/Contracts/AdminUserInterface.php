<?php

namespace Sentrio\Database\Contracts;

use Sentrio\Database\Models\AdminUser;

interface AdminUserInterface extends BaseInterface
{
    public function findByEmail(string $email): AdminUser;
}
