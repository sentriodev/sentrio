<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface PropertyInterface extends BaseInterface
{
    public function allPropertyToUseInProduct(): Collection;
}
