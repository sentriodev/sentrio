<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\TaxGroup;

interface TaxGroupInterface
{
    public function create(array $data): TaxGroup;

    public function all(): Collection;
}
