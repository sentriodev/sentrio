<?php

namespace Sentrio\Database\Contracts;

use Sentrio\Database\Models\OrderStatus;

interface OrderStatusInterface extends BaseInterface
{
    public function findDefault(): OrderStatus;
}
