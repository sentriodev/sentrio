<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use Sentrio\Database\Models\Product;

interface ProductInterface
{
    public function findBySlug(string $slug): Product;

    public function findByBarcode(string $barcode): Product;

    public function getAllWithoutVariation(int $perPage): LengthAwarePaginator;
}
