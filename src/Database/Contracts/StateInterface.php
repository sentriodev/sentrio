<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\State;

interface StateInterface
{
    public function create(array $data): State;

    public function find(int $id): State;

    public function delete(int $id): int;

    public function all(): Collection;
}
