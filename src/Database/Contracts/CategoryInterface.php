<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as SupportCollection;
use Sentrio\Database\Models\Category;

interface CategoryInterface extends BaseInterface
{
    public function options(): SupportCollection;

    public function findBySlug(string $slug): Category;

    // public function getCategoryProducts(Request $request): Collection;

    // public function getCategoryOptionForMenuBuilder(): SupportCollection;
}
