<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Support\Collection;

interface MenuGroupInterface extends BaseInterface
{
    public function getTreeByIdentifier(string $identifier): Collection;
}
