<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface OrderInterface extends BaseInterface
{
    public function findByUserId(int $id): Collection;

    public function getCurrentMonthTotalOrder(): int;

    public function getCurrentMonthTotalRevenue(): float;
}
