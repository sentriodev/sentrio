<?php

namespace Sentrio\Database\Contracts;

use Sentrio\Database\Models\Customer;

interface CustomerInterface extends BaseInterface
{
    public function findByEmail(string $email): ?Customer;
}
