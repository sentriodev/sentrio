<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\Address;

interface AddressInterface
{
    public function create(array $data): Address;

    public function find(int $id): Address;

    public function delete(int $id): int;

    public function all(): Collection;

    public function getByUserId(int $userId): Collection;
}
