<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Support\Collection;
use Sentrio\Database\Models\Role;

interface RoleInterface extends BaseInterface
{
    public function findAdminRole(): Role;

    public function options(): Collection;
}
