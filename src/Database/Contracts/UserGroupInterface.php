<?php

namespace Sentrio\Database\Contracts;

use Sentrio\Database\Models\UserGroup;

interface UserGroupInterface extends BaseInterface
{
    public function getIsDefault(): UserGroup;
}
