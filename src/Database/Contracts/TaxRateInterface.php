<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\TaxRate;

interface TaxRateInterface
{
    public function create(array $data): TaxRate;

    public function all(): Collection;
}
