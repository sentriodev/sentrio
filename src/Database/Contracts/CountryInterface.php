<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Support\Collection;

interface CountryInterface
{
    public function options(): Collection;
}
