<?php

namespace Sentrio\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Models\Menu;

interface MenuInterface
{
    public function create(array $data): Menu;

    public function find(int $id): Menu;

    public function delete(int $id): int;

    public function all(): Collection;
}
