<?php

namespace Sentrio\Database\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFilter extends Model
{
    public const TYPE_ATTRIBUTE = 'ATTRIBUTE';

    public const TYPE_PROPERTY = 'PROPERTY';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'filter_id', 'type'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getFilterAttribute()
    {
        if (self::TYPE_PROPERTY === $this->type) {
            return Property::find($this->filter_id);
        } else {
            return Attribute::find($this->filter_id);
        }
    }
}
