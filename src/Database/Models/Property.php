<?php

namespace Sentrio\Database\Models;

class Property extends BaseModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name'.
        'slug',
        'data_type',
        'field_type',
        'use_for_all_products',
        'is_visible_frontend',
        'use_for_category_filter',
        'sort_order'
    ];

    const DATA_TYPES = [
        'INTEGER' => 'Integer',
        'DECIMAL' => 'Decimal',
        'DATETIME' => 'Date Time',
        'VARCHAR' => 'Varchar (max:255)',
        'BOOLEAN' => 'Boolean (true/false)',
        'TEXT' => 'Textarea'
    ];

    const FIELD_TYPES = [
        'TEXT' => 'Textbox',
        'TEXTAREA' => 'Textarea',
    ];
}
