<?php

namespace Sentrio\Database\Models;

class Role extends BaseModel
{
    const ADMIN = 'Administrator';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function hasPermission($routes)
    {
        $models = $this->permissions->pluck('name');
        $permissions = explode(',', $routes);
        $flag = true;

        foreach ($permissions as $permissions) {
            if (! $models->contains($permissions)) {
                $flag = false;
            }
        }

        return $flag;
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
