<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Support\Collection;
use Sentrio\Database\Contracts\MenuGroupInterface;
use Sentrio\Database\Models\MenuGroup;

class MenuGroupRepository extends BaseRepository implements MenuGroupInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new MenuGroup();
    }

    public function model(): MenuGroup
    {
        return $this->model;
    }

    public function getTreeByIdentifier(string $identifier): Collection
    {
        $menus = collect();

        $menuGroup = MenuGroup::whereIdentifier($identifier)->first();
        if ($menuGroup !== null) {
            $modelMenus = $menuGroup->menus()->whereNull('parent_id')->get();
            foreach ($modelMenus as $modelMenu) {
                $modelMenu->subMenus;
                $menus->push($modelMenu);
            }
        }

        return $menus;
    }
}
