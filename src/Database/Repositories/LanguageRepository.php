<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\LanguageInterface;
use Sentrio\Database\Models\Language;

class LanguageRepository extends BaseRepository implements LanguageInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Language();
    }

    public function model(): Language
    {
        return $this->model;
    }
}
