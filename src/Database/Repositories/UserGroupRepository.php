<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\UserGroupInterface;
use Sentrio\Database\Models\UserGroup;

class UserGroupRepository extends BaseRepository implements UserGroupInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new UserGroup();
    }

    public function model(): UserGroup
    {
        return $this->model;
    }

    public function getIsDefault(): UserGroup
    {
        return UserGroup::whereIsDefault(true)->first();
    }
}
