<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\MenuInterface;
use Sentrio\Database\Models\Menu;

class MenuRepository implements MenuInterface
{
    public function create(array $data): Menu
    {
        return Menu::create($data);
    }

    public function find(int $id): Menu
    {
        return Menu::find($id);
    }

    public function delete(int $id): int
    {
        return Menu::destroy($id);
    }

    public function all(): Collection
    {
        return Menu::all();
    }
}
