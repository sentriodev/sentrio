<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\AdminUserInterface;
use Sentrio\Database\Models\AdminUser;

class AdminUserRepository extends BaseRepository implements AdminUserInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new AdminUser();
    }

    public function model(): AdminUser
    {
        return $this->model;
    }

    public function findByEmail(string $email): AdminUser
    {
        return AdminUser::whereEmail($email)->first();
    }
}
