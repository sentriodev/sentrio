<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Support\Collection;
use Sentrio\Database\Contracts\CountryInterface;
use Sentrio\Database\Models\Country;

class CountryRepository implements CountryInterface
{
    public function options(): Collection
    {
        return Country::all()->pluck('name', 'id');
    }

    public function currencyCodeOptions(): Collection
    {
        return Country::all()->pluck('currency_code', 'currency_code');
    }

    public function currencySymbolOptions(): Collection
    {
        return Country::all()->pluck('currency_symbol')->unique();
    }
}
