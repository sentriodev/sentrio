<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\PermissionInterface;
use Sentrio\Database\Models\Permission;

class PermissionRepository implements PermissionInterface
{
    public function create(array $data): Permission
    {
        return Permission::create($data);
    }

    public function find(int $id): Permission
    {
        return Permission::find($id);
    }

    public function all(): Collection
    {
        return Permission::all();
    }

    public function findByName(string $name)
    {
        return Permission::whereName($name)->first();
    }
}
