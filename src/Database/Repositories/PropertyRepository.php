<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\PropertyInterface;
use Sentrio\Database\Models\Property;

class PropertyRepository extends BaseRepository implements PropertyInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Property();
    }

    public function model(): Property
    {
        return $this->model;
    }

    public function create(array $data): Property
    {
        return Property::create($data);
    }

    public function find(int $id): Property
    {
        return Property::find($id);
    }

    public function delete(int $id): int
    {
        return Property::destroy($id);
    }

    public function all(): Collection
    {
        return Property::all();
    }

    public function allPropertyToUseInProduct(): Collection
    {
        return Property::whereUseForAllProducts(1)->get();
    }
}
