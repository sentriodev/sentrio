<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\CurrencyInterface;
use Sentrio\Database\Models\Currency;

class CurrencyRepository extends BaseRepository implements CurrencyInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Currency();
    }

    public function model(): Currency
    {
        return $this->model;
    }

    public function create(array $data): Currency
    {
        return Currency::create($data);
    }

    public function find(int $id): Currency
    {
        return Currency::find($id);
    }

    public function delete(int $id): int
    {
        return Currency::destroy($id);
    }

    public function all(): Collection
    {
        return Currency::all();
    }
}
