<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\CustomerInterface;
use Sentrio\Database\Models\Customer;

class CustomerRepository extends BaseRepository implements CustomerInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Customer();
    }

    public function model(): Customer
    {
        return $this->model;
    }

    public function findByEmail(string $email): ?Customer
    {
        return Customer::whereEmail($email)->first();
    }
}
