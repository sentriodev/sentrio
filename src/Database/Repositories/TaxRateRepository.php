<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\TaxRateInterface;
use Sentrio\Database\Models\TaxRate;

class TaxRateRepository implements TaxRateInterface
{
    public function create(array $data): TaxRate
    {
        return TaxRate::create($data);
    }

    public function all(): Collection
    {
        return TaxRate::all();
    }
}
