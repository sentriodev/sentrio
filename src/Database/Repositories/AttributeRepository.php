<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\AttributeInterface;
use Sentrio\Database\Models\Attribute;

class AttributeRepository extends BaseRepository implements AttributeInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Attribute();
    }

    public function model(): Attribute
    {
        return $this->model;
    }
}
