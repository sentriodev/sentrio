<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\StateInterface;
use Sentrio\Database\Models\State;

class StateRepository implements StateInterface
{
    public function create(array $data): State
    {
        return State::create($data);
    }

    public function find(int $id): State
    {
        return State::find($id);
    }

    public function delete(int $id): int
    {
        return State::destroy($id);
    }

    public function all(): Collection
    {
        return State::all();
    }
}
