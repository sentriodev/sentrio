<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\AddressInterface;
use Sentrio\Database\Models\Address;

class AddressRepository implements AddressInterface
{
    public function create(array $data): Address
    {
        return Address::create($data);
    }

    public function find(int $id): Address
    {
        return Address::find($id);
    }

    public function delete(int $id): int
    {
        return Address::destroy($id);
    }

    public function all(): Collection
    {
        return Address::all();
    }

    public function getByUserId(int $userId): Collection
    {
        return Address::whereUserId($userId)->get();
    }
}
