<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as SupportCollection;
use Sentrio\Database\Contracts\CategoryInterface;
use Sentrio\Database\Models\Attribute;
use Sentrio\Database\Models\Category;
use Sentrio\Database\Models\Product;
use Sentrio\Database\Models\Property;

class CategoryRepository extends BaseRepository implements CategoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Category();
    }

    public function model(): Category
    {
        return $this->model;
    }

    public function options(): SupportCollection
    {
        return Category::all()->pluck('name', 'id');
    }

    public function findBySlug(string $slug): Category
    {
        return Category::whereSlug($slug)->first();
    }

    public function getCategoryProducts(Request $request): Collection
    {
        $builder = Product::whereHas('categories', function ($query) use ($request) {
            $query->whereSlug($request->get('slug'));
        });

        return $builder->get();
    }

    // public function getCategoryOptionForMenuBuilder(): SupportCollection

    protected function filterProperties($builder, $suffix, $values)
    {
        $properties = Property::whereSlug($suffix)->first();
    }

    protected function filterAttributes($builder, $suffix, $values)
    {
        $attribute = Attribute::whereSlug($suffix)->first();
    }

    /**
     * Split the parameter and find out the type
     *
     * @param  string $key
     * @return array
     */
    protected function splitParameter($key)
    {
        $filterType = '';
        $prefix = substr($key, 0, 4);
        $suffix = substr($key, 4);

        if ($prefix === 'p___') {
            $filterType = 'PROPERTY';
        } elseif ($prefix === 'a___') {
            $filterType = 'ATTRIBUTE';
        }

        return [$filterType, $suffix];
    }
}
