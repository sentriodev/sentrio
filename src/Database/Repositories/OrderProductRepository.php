<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\OrderProductInterface;
use Sentrio\Database\Models\OrderProduct;
use Sentrio\Order\Events\OrderProductCreated;

class OrderProductRepository extends BaseRepository implements OrderProductInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new OrderProduct();
    }

    public function model(): OrderProduct
    {
        return $this->model;
    }

    public function create(array $data): OrderProduct
    {
        $orderProduct = parent::create($data);

        event(new OrderProductCreated($orderProduct));

        return $orderProduct;
    }
}
