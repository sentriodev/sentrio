<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Support\Collection;
use Sentrio\Database\Contracts\RoleInterface;
use Sentrio\Database\Models\Role;

class RoleRepository extends BaseRepository implements RoleInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function model(): Role
    {
        return $this->model;
    }

    public function findAdminRole(): Role
    {
        return Role::whereName(Role::ADMIN)->first();
    }

    public function options(): Collection
    {
        return Role::all()->pluck('name', 'id');
    }
}
