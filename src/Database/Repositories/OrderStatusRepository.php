<?php

namespace Sentrio\Database\Repositories;

use Sentrio\Database\Contracts\OrderStatusInterface;
use Sentrio\Database\Models\OrderStatus;

class OrderStatusRepository extends BaseRepository implements OrderStatusInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new OrderStatus();
    }

    public function model(): OrderStatus
    {
        return $this->model;
    }

    public function findDefault(): OrderStatus
    {
        return OrderStatus::whereIsDefault(1)->first();
    }
}
