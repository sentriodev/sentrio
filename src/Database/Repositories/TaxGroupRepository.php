<?php

namespace Sentrio\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Sentrio\Database\Contracts\TaxGroupInterface;
use Sentrio\Database\Models\TaxGroup;

class TaxGroupRepository implements TaxGroupInterface
{
    public function create(array $data): TaxGroup
    {
        return TaxGroup::create($data);
    }

    public function all(): Collection
    {
        return TaxGroup::all();
    }
}
