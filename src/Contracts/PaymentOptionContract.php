<?php

namespace Sentrio\Contracts;

use Sentrio\Database\Models\Address;
use Sentrio\Database\Models\Order;

interface PaymentOptionContract
{
    public function identifier();

    public function name();

    public function requiresAddress();

    public function setAddress(Address $address);

    public function address();

    public function process();

    public function syncOrder(Order $order);

    public function view();

    public function render();

    public function with();
}
