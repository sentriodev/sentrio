<?php

namespace Sentrio\Contracts;

interface BreadcrumbContract
{
    public function label();

    public function route();
}
