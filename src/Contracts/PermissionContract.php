<?php

namespace Sentrio\Contracts;

interface PermissionContract
{
    public function key($key = null);

    public function label($label = null);

    public function routes($routeName = null);
}
