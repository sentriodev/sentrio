<?php

namespace Sentrio\Contracts;

interface WidgetContract
{
    public function label();

    public function type();
}
