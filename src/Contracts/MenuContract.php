<?php

namespace Sentrio\Contracts;

interface MenuContract
{
    public function key($key = null);

    public function label($label = null);

    public function icon($icon = null);

    public function attributes($attributes = []);

    public function route($routeName = null);
}
