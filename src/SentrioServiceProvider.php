<?php

namespace Sentrio;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Sentrio\Console\AdminCommand;
use Sentrio\Console\InstallCommand;
use Sentrio\Http\Middleware\AdminAuthenticate;
use Sentrio\Http\Middleware\Permissionable;
use Sentrio\Http\Middleware\RedirectIfAdminAuthenticated;
use Sentrio\Http\Middleware\SetDefaultCurrency;
use Sentrio\View\Composers\LayoutComposer;

class SentrioServiceProvider extends ServiceProvider
{
    /**
     * The package service provider mappings
     *
     * @var array
     */
    protected $providers = [
        \Sentrio\Support\Providers\BreadcrumbServiceProvider::class,
        \Sentrio\Support\Providers\EventServiceProvider::class,
        \Sentrio\Support\Providers\PermissionServiceProvider::class,
        \Sentrio\Support\Providers\RepositoryServiceProvider::class,
        \Sentrio\Support\Providers\WidgetServiceProvider::class
    ];

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerConfig();
        $this->registerRoutes();
        $this->registerMiddlewares();
        $this->registerViewComposer();
        $this->registerCommands();
        $this->registerMigrations();
        $this->registerViews();
    }

    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot()
    {
        $this->bootTranslations();
        $this->bootPublishing();
    }

    /**
     * Register the package service providers
     *
     * @return void
     */
    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            App::register($provider);
        }
    }

    /**
     * Register the Sentrio configuration
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/sentrio.php', 'sentrio');

        $config = $this->app->make('config');

        $sentrio = include __DIR__.'/../config/sentrio.php';
        $fs = $config->get('filesystems', []);
        $auth = $config->get('auth', []);

        $config->set('filesystems', array_merge($fs, $sentrio['filesystems']));
        $config->set('auth.guards', array_merge($auth['guards'], $sentrio['auth']['guards']));
        $config->set('auth.providers', array_merge($auth['providers'], $sentrio['auth']['providers']));
        $config->set('auth.passwords', array_merge($auth['passwords'], $sentrio['auth']['passwords']));
    }

    /**
     * Register the package route
     *
     * @return void
     */
    protected function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }

    /**
     * Register the package middlewares
     *
     * @return void
     */
    protected function registerMiddlewares()
    {
        $router = $this->app['router'];
        $router->aliasMiddleware('admin.auth', AdminAuthenticate::class);
        $router->aliasMiddleware('permission', Permissionable::class);
        $router->aliasMiddleware('admin.guest', RedirectIfAdminAuthenticated::class);
        $router->aliasMiddleware('sentrio', SetDefaultCurrency::class);
    }

    /**
     * Register the package view composer
     * 
     * @return void
     */
    protected function registerViewComposer()
    {
        View::composer('sentrio::layouts.app', LayoutComposer::class);
    }

    /**
     * Register the package console commands
     *
     * @return void
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                // AdminCommand::class
            ]);
        }
    }

    /**
     * Register the package migrations
     *
     * @return void
     */
    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    /**
     * Register the package view resources
     *
     * @return void
     */
    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'sentrio');
    }

    /**
     * Bootstrap the package translation resources
     *
     * @return void
     */
    protected function bootTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'sentrio');
    }

    /**
     * Bootstrap the package publishables
     *
     * @return void
     */
    protected function bootPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/sentrio.php' => config_path('sentrio.php')
            ], 'sentrio-config');

            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/sentrio')
            ], 'sentrio-assets');
        }
    }
}
