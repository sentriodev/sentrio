<?php

namespace Sentrio\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Sentrio\Database\Contracts\CurrencyInterface;

class SetDefaultCurrency
{
    /**
     * The currency repository instance
     *
     * @var \Sentrio\Database\Contracts\CurrencyInterface
     */
    protected $currencyRepository;

    /**
     * Create new instance
     *
     * @param  \Sentrio\Database\Contracts\CurrencyInterface $currencyRepository
     * @return void
     */
    public function __construct(CurrencyInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Handle an incoming request
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return void
     */
    public function handle($request, Closure $next)
    {
        $this->setDefaultCurrency();

        return $next($request);
    }

    /**
     * Set the default currency of the application
     *
     * @return void
     */
    public function setDefaultCurrency()
    {
        if (! Session::has('default_currency')) {
            $currency = $this->currencyRepository->all()->first();

            Session::put('default_currency', $currency);
        }
    }
}
