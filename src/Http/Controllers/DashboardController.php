<?php

namespace Sentrio\Http\Controllers;

use Sentrio\Support\Facades\Widget;

class DashboardController
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $customerWidget = Widget::get('sentrio-total-customer');
        $orderWidget = Widget::get('sentrio-total-order');
        $revenueWidget = Widget::get('sentrio-total-revenue');

        return view('sentrio::admin', compact('customerWidget', 'orderWidget', 'revenueWidget'));
    }
}
