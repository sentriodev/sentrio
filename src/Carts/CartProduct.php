<?php

namespace Sentrio\Carts;

use Sentrio\Contracts\CartProductContract;

class CartProduct implements CartProductContract
{
    protected $id;

    protected $name;

    protected $attributes;

    protected $slug;

    protected $qty;

    protected $price;

    protected $taxAmount = 0;

    protected $image;

    public function name($name = null)
    {
        if ($name === null) {
            return $this->name;
        } else {
            $this->name = $name;

            return $this;
        }
    }

    public function id($id = null)
    {
        if ($id === null) {
            return $this->id;
        } else {
            $this->id = $id;

            return $this;
        }
    }

    public function taxAmount($taxAmount = null)
    {
        if ($taxAmount === null) {
            return $this->taxAmount;
        } else {
            $this->taxAmount = $taxAmount;

            return $this;
        }
    }

    public function attributes($attributes = null)
    {
        if ($attributes === null) {
            return $this->attributes;
        } else {
            $this->attributes = $attributes;

            return $this;
        }
    }

    public function image($image = null)
    {
        if ($image === null) {
            return $this->image;
        } else {
            $this->image = $image;

            return $this;
        }
    }

    public function slug($slug = null)
    {
        if ($slug === null) {
            return $this->slug;
        } else {
            $this->slug = $slug;

            return $this;
        }
    }

    public function qty($qty = null)
    {
        if ($qty === null) {
            return $this->qty;
        } else {
            $this->qty = $qty;

            return $this;
        }
    }

    public function price($price = null)
    {
        if ($price === null) {
            return $this->price;
        } else {
            $this->price = $price;

            return $this;
        }
    }

    public function total(): float
    {
        return ($this->qty() * $this->price()) + $this->taxAmount();
    }
}
