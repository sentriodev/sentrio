<?php

namespace Sentrio\Carts;

use Illuminate\Session\SessionManager;
use Illuminate\Support\Collection;
use Sentrio\Database\Contracts\ProductInterface;
use Sentrio\Database\Models\Product;

class Manager
{
    /**
     * The product repository instance
     *
     * @var \Sentrio\Database\Contracts\ProductInterface
     */
    protected $productRepository;

    /**
     * The cart product collection
     *
     * @var \Illuminate\Support\Collection
     */
    protected $cartCollection;

    /**
     * The cart product total discount
     *
     * @var float
     */
    protected $totalDiscount = 0.00;

    /**
     * The session manager instance
     *
     * @var \Illuminate\Session\SessionManager
     */
    public $session;

    /**
     * Create a new cart manager instance
     *
     * @param  \Illuminate\Session\SessionManager $session
     */
    public function __construct(SessionManager $session)
    {
        $this->productRepository = app(ProductInterface::class);
        $this->session = $session;
        $this->cartCollection = $this->getSession();
    }

    /**
     * Delete an item from the collection by slug
     *
     * @param  string $slug
     * @return self
     */
    public function destroy(string $slug)
    {
        $this->cartCollection->pull($slug);

        return $this;
    }

    /**
     * Update the cart product from the collection by slug and quantity
     *
     * @param  string $slug
     * @param  mixed  $qty
     * @return self
     */
    public function update(string $slug, $qty)
    {
        $product = $this->cartCollection->get($slug);
        $product->qty($qty);

        $this->cartCollection->put($slug, $product);
        $this->updateSessionCollection();

        return $this;
    }

    public function add(string $slug, $qty = 1, $attributes = [])
    {
        $status = false;
        $message = '';
        $product = $this->productRepository->findBySlug($slug);

        if ($product->type === Product::PRODUCT_BASIC && $qty > (float) $product->qty) {
            return [false, __('sentrio::core.notification.insufficient_qty')];
        }

        if ($this->exists($slug)) {
            $cartProduct = $this->cartCollection->get($slug);
            $existingQty = $cartProduct->qty() ?? 0;
            $newQty = $qty + $existingQty;

            if ($newQty > $product->qty) {
                $status = false;
                $message = __('sentrio::core.notification.insufficient_qty');
            } else {
                $cartProduct->qty($qty + $existingQty);

                $this->cartCollection->put($slug, $cartProduct);
                $this->updateSessionCollection();

                $status = true;
                $message = __('sentrio::catalog.cart_success');
            }
        } elseif ($product->type === 'VARIABLE_PRODUCT') {
            //
        } else {
            $cartProduct = $this->createCartProductFromSlug($product);
            $cartProduct->qty($qty);

            $this->cartCollection->put($slug, $cartProduct);
            $this->updateSessionCollection();

            $status = true;
            $message = __('sentrio::catalog.cart_success');
        }

        return [$status, $message];
    }

    /**
     * Create a cart product from slug
     *
     * @param  \Sentrio\Database\Models\Product $product
     * @param  mixed                           $attributes
     * @return \Sentrio\Carts\CartProduct
     */
    public function createCartProductFromSlug(Product $product, $attributes = null): CartProduct
    {
        $cartProduct = new CartProduct;
        $cartProduct
            ->name($product->name)
            ->id($product->id)
            ->slug($product->slug)
            ->price($product->getPrice())
            ->taxAmount($product->getTaxAmount())
            ->attributes($attributes ?? [])
            ->image($product->main_image_url);

        return $cartProduct;
    }

    /**
     * Check if an item exists in the collection
     *
     * @param  string $slug
     * @return bool
     */
    protected function exists($slug): bool
    {
        return $this->getSession()->has($slug);
    }

    /**
     * Clear all the session key from the collection
     *
     * @return void
     */
    public function clear()
    {
        $this->session->forget($this->getSessionKey());
    }

    /**
     * Return all of the session from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->getSession();
    }

    /**
     * Get the session
     *
     * @return \Illuminate\Support\Collection|string
     */
    public function getSession()
    {
        $key = $this->getSessionKey();

        if ($this->session->has($key)) {
            return $this->session->get($key);
        } else {
            return new Collection;
        }
    }

    /**
     * Get the key for the session
     *
     * @return string
     */
    public function getSessionKey()
    {
        return config('sentrio.cart.session_key') ?? 'cart_discount';
    }

    /**
     * Update the session collection
     *
     * @return self
     */
    protected function updateSessionCollection()
    {
        $this->session->put($this->getSessionKey(), $this->cartCollection);

        return $this;
    }

    /**
     * Get the list of all cart product items
     *
     * @return \Illuminate\Support\Collection
     */
    public function toArray()
    {
        $products = $this->all();
        $items = Collection::make([]);

        foreach ($products as $product) {
            $items->push([
                'slug' => $product->slug(),
                'image' => $product->image(),
                'price' => $product->price(),
                'qty' => $product->qty(),
                'name' => $product->name(),
                'tax' => $product->taxAmount(),
                'attributes' => $product->attributes()
            ]);
        }

        return $items;
    }

    /**
     * Get the cart product total
     *
     * @param  bool $format
     * @return mixed
     */
    public function total($format = true)
    {
        $products = $this->all();
        $total = 0;

        foreach ($products as $product) {
            $total += $product->total();
        }

        $total = $total - $this->totalDiscount;

        if ($format === true) {
            return number_format($total, 2);
        }
    }
}
