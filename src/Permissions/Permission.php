<?php

namespace Sentrio\Permissions;

use Illuminate\Support\Facades\Lang;
use Sentrio\Contracts\PermissionContract;

class Permission implements PermissionContract
{
    protected $label;

    protected $routes;

    protected $key;

    public function __construct($callable = null)
    {
        if (null !== $callable) {
            $callable($this);
        }
    }

    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        if (Lang::has($this->label)) {
            return __($this->label);
        }

        return $this->label;
    }

    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }

    public function routes($routes = null)
    {
        if (null !== $routes) {
            $this->routes = $routes;

            return $this;
        }

        return $this->routes;
    }
}
