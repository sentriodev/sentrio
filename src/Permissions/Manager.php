<?php

namespace Sentrio\Permissions;

use Illuminate\Support\Collection;

class Manager
{
    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create a new breadcrumb manager instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    /**
     * Return all item from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->collection;
    }

    /**
     * Set an item in the collection
     *
     * @param  string $key
     * @param  mixed $permission
     * @return $this
     */
    public function set($key, $permission)
    {
        $this->collection->put($key, $permission);

        return $this;
    }

    /**
     * Get an item from the collection by key
     *
     * @param  string $key
     * @return \Illuminate\Support\Collection
     */
    public function get($key)
    {
        if ($this->collection->has($key)) {
            return $this->collection->get($key);
        }

        return Collection::make([]);
    }

    /**
     * Add group to a permission in the collection
     *
     * @param  string $key
     * @param  string|callable $callable
     * @return \Sentrio\Permissions\PermissionGroup
     */
    public function add($key, $callable = null)
    {
        if (null !== $callable) {
            $group = new PermissionGroup($callable);
            $group->key($key);

            $this->collection->put($key, $group);
        } else {
            $group = new PermissionGroup();
            $group->key($key);

            $this->collection->put($key, $group);
        }

        return $group;
    }
}
