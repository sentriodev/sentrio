<?php

namespace Sentrio\Permissions;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;

class PermissionGroup
{
    /**
     * The permission group label
     *
     * @var string
     */
    protected $label;

    /**
     * The permission group key
     *
     * @var string
     */
    protected $key;

    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    public $collection;

    /**
     * Create new permission group instance
     *
     * @param  callable|null $callable
     * @return void
     */
    public function __construct($callable = null)
    {
        $this->collection = Collection::make([]);

        if (null !== $callable) {
            $callable($this);
        }
    }

    /**
     * Return the permission group label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        if (Lang::has($this->label)) {
            return __($this->label);
        }

        return $this->label;
    }

    /**
     * Return the permission group key
     *
     * @param  string|null $key
     * @return mixed
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }

    /**
     * Add a permission to an item
     *
     * @param  string $key
     * @param  callable|null $callable
     * @return \Sentrio\Permissions\Permission
     */
    public function addPermission($key, $callable = null)
    {
        if (null !== $callable) {
            $permission = new Permission($callable);
            $permission->key($key);

            $this->collection->put($key, $permission);
        } else {
            $permission = new Permission();
            $permission->key($key);

            $this->collection->put($key, $permission);
        }

        return $permission;
    }
}
