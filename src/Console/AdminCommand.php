<?php

namespace Sentrio\Console;

use Illuminate\Console\Command;
use Sentrio\Database\Models\Role;
use Sentrio\Support\Traits\InstallHelper;

class AdminCommand extends Command
{
    use InstallHelper;

    /**
     * The name and signature of the console command
     *
     * @var string
     */
    protected $signature = 'sentrio:admin';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Create application Administrator';

    /**
     * Execute the console command
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Create Admin User for Sentrio Administration Panel');
        $this->createAdminUser();
        $this->info('Admin User created successfully!');
    }

    /**
     * Create admin user
     *
     * @return void
     */
    protected function createAdminUser()
    {
        $data['first_name'] = $this->ask('First Name', 'John');
        $data['last_name'] = $this->ask('Last Name', 'Doe');
        $data['email'] = $this->ask('Email Address', 'john@doe.com');
        $data['password'] = $this->secret('Password');
        $data['confirm_password'] = $this->secret('Confirm Password');

        // check if passwords match
        if ($data['password'] !== $data['confirm_password']) {
            $this->warn('Passwords do not match!');
        }

        $role = $this->roleRepository->findAdminRole();

        $data['role_id'] = $role->id;
        $data['is_super_admin'] = 1;
        $data['password'] = bcrypt($data['password']);

        $this->adminUserRepository->create($data);

        $this->info('Creating admin account...');
    }
}
