<?php

namespace Sentrio\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Sentrio\Database\Models\Role;
use Sentrio\SentrioServiceProvider;
use Sentrio\Support\Traits\InstallHelper;

class InstallCommand extends Command
{
    use InstallHelper;

    /**
     * The progress bar
     *
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    protected $progressBar;

    /**
     * The console command signature
     *
     * @var string
     */
    protected $signature = 'sentrio:install';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Install the Sentrio eCommerce resources';

    /**
     * Execute the console command
     *
     * @return void
     */
    public function handle()
    {
        $this->progressBar = $this->output->createProgressBar(3);

        $this->info('Starting installation. Please wait...');
        sleep(1);

        $this->info('Installation of Sentrio package, setup database, publish assets and config files.');

        if (! $this->progressBar->getProgress()) {
            $this->progressBar->start();
        }

        $this->call('vendor:publish', ['--provider' => SentrioServiceProvider::class]);
        $this->progressBar->advance();

        $this->setUpDatabase();

        $this->info('Creating a symbolic link...');
        $this->call('storage:link');
        $this->progressBar->advance();

        $this->bootDefaults();

        $this->info('Creating Admin User...');
        $this->call('sentrio:admin');

        $this->completeSetup();
    }

    protected function setUpDatabase()
    {
        $this->info('Migrating the database tables into your application');
        $this->call('migrate:fresh');
        $this->progressBar->advance();

        // visually slow down the installation process so that the user can read whats happening
        usleep(350000);
    }

    protected function installDummyData()
    {
        $this->info('Populating data...');

        if ($this->confirm('Would you like to install dummy data?')) {
            $this->call('sentrio:module --install', ['identifier' => 'dummy-data']);
        }
    }

    /**
     * Bootstrap the necessary data
     *
     * @return void
     */
    protected function bootDefaults()
    {
        //
    }

    protected function completeSetup()
    {
        $this->progressBar->finish();

        $this->info('Installation Complete!');
    }

    private function configExists($file)
    {
        if (! File::isDirectory(config_path($file))) {
            return false;
        }

        return ! empty(File::allFiles(config_path($file)));
    }
}
