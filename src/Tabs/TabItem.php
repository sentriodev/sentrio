<?php

namespace Sentrio\Tabs;

class TabItem
{
    /**
     * The tab item label
     *
     * @var string
     */
    protected $label;

    /**
     * The tab item view path
     *
     * @var string
     */
    protected $view;

    /**
     * The tab item key
     *
     * @var string
     */
    protected $key;

    /**
     * Create a new instance
     *
     * @param  callable $callable
     * @return void
     */
    public function __construct(callable $callable)
    {
        $callable($this);
    }

    /**
     * Return a tab item label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        return trans($this->label);
    }

    /**
     * Return a tab item view path
     *
     * @param  string|null $view
     * @return mixed
     */
    public function view($view = null)
    {
        if (null !== $view) {
            $this->view = $view;

            return $this;
        }

        return $this->view;
    }

    /**
     * Return a tab item by key
     *
     * @param  string|null $key
     * @return mixed
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }
}
