<?php

namespace Sentrio\Tabs;

use Illuminate\Support\Collection;

class TabManager
{
    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    public $collection;

    /**
     * Create a new tab manager instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    /**
     * Return all of the items from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(): Collection
    {
        return $this->collection;
    }

    /**
     * Get an item from the collection by key
     *
     * @param  string $key
     * @return \Illuminate\Support\Collection
     */
    public function get($key): Collection
    {
        return $this->collection->get($key);
    }

    /**
     * Put an item in the collection by key
     *
     * @param  string   $key
     * @param  callable $tab
     * @return $this
     */
    public function put(string $key, callable $tab)
    {
        $obj = new TabItem($tab);

        if (! $this->collection->has($key)) {
            $collection = Collection::make([]);
            $collection->push($obj);
        } else {
            $collection = $this->collection->get($key);
            $collection->push($obj);
        }

        $this->collection->put($key, $collection);

        return $this;
    }
}
