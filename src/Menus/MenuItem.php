<?php

namespace Sentrio\Menus;

use Sentrio\Contracts\MenuContract;

class MenuItem implements MenuContract
{
    const IS_ADMIN = 'admin';

    const IS_FRONT = 'front';

    /**
     * The menu item label
     *
     * @var string
     */
    public $label;

    /**
     * The menu item type
     *
     * @var string
     */
    public $type;

    /**
     * The menu item icon
     *
     * @var string
     */
    public $icon;

    /**
     * The menu item attributes
     *
     * @var string
     */
    public $attributes;

    /**
     * The menu item key
     *
     * @var string
     */
    public $key;

    /**
     * The menu item parameters
     *
     * @var string
     */
    public $parameters;

    /**
     * The menu item route name
     *
     * @var string
     */
    public $routeName;

    /**
     * The menu item callback
     *
     * @var callable|null
     */
    public $callback;

    /**
     * The menu item sub menu
     *
     * @var array
     */
    public $subMenu;

    /**
     * Create new menu item instance
     *
     * @param  callable $callable
     */
    public function __construct($callable)
    {
        $this->callback = $callable;
        $callable($this);
    }

    /**
     * Return the menu item label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        return trans($this->label);
    }

    /**
     * Return the menu item type
     *
     * @param  string|null $type
     * @return mixed
     */
    public function type($type = null)
    {
        if (null !== $type) {
            $this->type = $type;

            return $this;
        }

        return $this->type;
    }

    /**
     * Return the menu item key
     *
     * @param  string|null $key
     * @return mixed
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }

    /**
     * Return the menu item route
     *
     * @param  string|null $routeName
     * @return mixed
     */
    public function route($routeName = null)
    {
        if (null !== $routeName) {
            $this->routeName = $routeName;

            return $this;
        }

        return $this->routeName;
    }

    /**
     * Return the menu item parameters
     *
     * @param  string|null $parameters
     * @return mixed
     */
    public function parameters($parameters = null)
    {
        if (null !== $parameters) {
            $this->parameters = $parameters;

            return $this;
        }

        return $this->parameters;
    }

    /**
     * Return the menu item icon
     *
     * @param  string|null $icon
     * @return mixed
     */
    public function icon($icon = null)
    {
        if (null !== $icon) {
            $this->icon = $icon;

            return $this;
        }

        return $this->icon;
    }

    /**
     * Return the menu item attributes
     *
     * @param  string|null $attributes
     * @return mixed
     */
    public function attributes($attributes = null)
    {
        if (null !== $attributes) {
            $this->attributes = $attributes;

            return $this;
        }

        return $this->attributes;
    }

    /**
     * Return the sub-menu of a menu
     *
     * @param  string|null $key
     * @param  mixed $item
     * @return $this
     */
    public function subMenu($key = null, $item = null)
    {
        if (is_null($item)) {
            return $this->subMenu;
        }

        $menu = new self($item);

        $this->subMenu[$key] = $menu;

        return $this;
    }

    /**
     * Check if a sub menu is present in a menu
     *
     * @return bool
     */
    public function hasSubMenu()
    {
        if (isset($this->subMenu) && count($this->subMenu) > 0) {
            return true;
        }

        return false;
    }
}
