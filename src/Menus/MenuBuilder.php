<?php

namespace Sentrio\Menus;

use Illuminate\Support\Collection;

class MenuBuilder
{
    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create new menu builder instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    /**
     * Create a new menu builder instance
     *
     * @param  string   $key
     * @param  callable $callable
     * @return $this
     */
    public function make($key, callable $callable)
    {
        $menu = new MenuItem($callable);
        $menu->key($key);

        $this->collection->put($key, $menu);

        return $this;
    }

    /**
     * Get an item from the collection by key
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->collection->get($key);
    }

    /**
     * Get all of the items in the collection
     *
     * @param  bool $admin
     * @return \Illuminate\Support\Collection
     */
    public function all($admin = false)
    {
        if ($admin) {
            return $this->collection->filter(function ($item) {
                return $item->type() === MenuItem::IS_ADMIN;
            });
        } else {
            return $this->collection->filter(function ($item) {
                return $item->type() === MenuItem::IS_FRONT;
            });
        }
    }

    public function getMenuFromRouteName($name)
    {
        $openKey = '';
        $itemKey = '';

        foreach ($this->collection as $key => $menuGroup) {
            if ($menuGroup->hasSubMenu()) {
                $subMenus = $menuGroup->subMenu($key);
                foreach ($subMenus as $subKey => $subMenu) {
                    if ($subMenu->route() == $name) {
                        $openKey = $key;
                        $itemKey = $subMenu->key();
                    }
                }
            }
        }

        return [$openKey, $itemKey];
    }

    public function frontMenus()
    {
        $frontMenus = collect();
        $i = 1;

        foreach ($this->collection as $item) {
            if ($item->type() === MenuItem::IS_FRONT) {
                $menu = new \stdClass;
                $menu->id = $i;
                $menu->name = $item->label;
                $menu->url = route($item->route(), $item->parameters());
                $menu->subMenus = $item->subMenus ?? [];

                $frontMenus->push($menu);
                $i++;
            }
        }

        return $frontMenus;
    }

    public function adminMenus()
    {
        $adminMenus = $this->all(true);

        $result = $adminMenus->map(function ($item, $index) {
            $routeName = $item->route();

            if ($item->hasSubMenu()) {
                $subMenus = collect($item->subMenu)->map(function ($item) {
                    $routeName = $item->route();

                    return [
                        'name' => $item->label(),
                        'url' => $routeName === '#' ? '#' : route($routeName, $item->parameters())
                    ];
                });
            }

            return [
                'name' => $item->label(),
                'icon' => $item->icon(),
                'url' => $routeName === '#' ? '#' : route($routeName, $item->parameters()),
                'subMenus' => $subMenus
            ];
        });

        return $result;
    }
}
