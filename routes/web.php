
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Sentrio\Http\Controllers\DashboardController;
use Sentrio\User\Controllers\ForgotPasswordController;
use Sentrio\User\Controllers\LoginController;
use Sentrio\User\Controllers\ResetPasswordController;

Route::group([
    'middleware' => ['web'],
    'prefix' => config('sentrio.prefix'),
    'namespace' => 'Sentrio',
    'as' => 'admin.'
], function () {
    // login / logout
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login.post');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

    // forgot password
    Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');

    // reset password
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
});

Route::group([
    'middleware' => ['web', 'admin.auth:admin', 'permission'],
    'prefix' => config('sentrio.prefix'),
    'as' => 'admin.',
    'namespace' => 'Sentrio'
], function () {
    // dashboard
    Route::get('', [DashboardController::class, 'index'])->name('dashboard');

    // resources
    Route::resource('admin-user', User\Controllers\AdminUserController::class);
    // attribute
    // category
    // currency
    // language
    // order
    // order status
    // menu group
    // property
    // product
    // role
    // state
    Route::resource('user-group', User\Controllers\UserGroupController::class);
    // tax group
    // tax rate
});
